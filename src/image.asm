; zx-spectrum-pong, (Wilso-Pong)
; Copyright (C) December 2017, Daniel Wilson
; 
; This program is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.
; 
; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
; 
; You should have received a copy of the GNU General Public License
; along with this program.  If not, see <https://www.gnu.org/licenses/>.




; This code is a system to display (in fullscreen) HMRLE compressed images.
; It includes the function image_fillScreen, where the image can be specified as a memory address of
; defined bytes representing the image.

; --------------------- SPECIFICATION FOR HMRLE COMPRESSION --------------------------------------
;                                         *****
; Copyright Daniel Wilson
; 22 December 2017
; Hybrid monochrome run length encoding
; 
; 
; This algorithm stores images using lossless compression.
; It uses run-length encoding to store the parts of the image that contain repeating parts, and stores
; the uncompressible parts of the image literally - i.e: Without compression.
; This means that a decent compression ratio is still achieved on images that have parts that cannot be compressed.
;
;
; BYTE SPEC
; ---------
;
; All data is assumed to be run-length encoded unless otherwise known.
; Each run-length encoded byte is stored as such:
; -> The most significant bit is stored as a zero or one, depending on the colour
; -> The 7 least significant bits are a number (1-127 inclusive) representing the number of times this colour is repeated.
;
; The byte values of 0x00 and 0x80 are impossible - i.e: Are not value run-length transition bytes.
; Byte value 0x80 is not used currently, but byte value 0x00 is used as such:
; 
; 
; BYTE 0x00
; ---------
;
; This represents that some literal (uncompressed) data has been stored.
; Following a 0x00 byte, two literal bytes follow. This is how uncompressible data is stored.
;





; Fills the screen with the image from the memory location specified in hl.
; The image should be encoded using HMRLE compression.
;
; (well-behaved)
;
; Parameters:
;       de: The memory location of the image to fill the screen with.
; Modifies:
;       hl, a, a'
; Returns:
;       NONE
;
image_fillScreen:
    push de ; Preserve de - this is incremented each time the loop is run, so the original value should
            ; be recovered at the end.
    push bc ; c is used to store y position
    
    ld bc, 0
    ;ld de, 0
    
    ld hl, SCREEN_MEMSTART ; Load hl with the screen memory starting location.
    
    imageLoop_fillScreen:
        
        push de ; Preserve de for this loop iteration - it is used in the byte processing methods as temporary data storage.
                ; de must be popped from the stack to increment it.
        
        ex de, hl
        ld a, (hl)    ; Get the next image byte (from location de) into hl, by exchanging.
        ex de, hl
        ld e, a
        
        cp 0
        jp z, image_ifUncomp
            ; If the image byte is not zero - it is a run-length encoded transition byte.
            call image_processByteCompressed ; So process it as a compressed byte.
            pop de ; Get the original value of de back
            inc de ; And then increment it.
        jp imageIfEndComp
        image_ifUncomp:
            ; If the image byte is zero - it signifies that the next two bytes stored literally, so:
            pop de ; Get the original value of de:
            ex de, hl   ; Swap de and hl
            inc hl      ; Increment hl - so increment the memory location within the image
            ld a, (hl)  ; Load the accumulator with the first literal image byte, then swap the accumulator with secondary
            ex af, af'
            inc hl      ; Increment hl again:
            ld a, (hl)  ; Load the accumulator with the second literal image byte, then swap the accumulator with secondary again.
            inc hl      ; Do a final hl increment, then retrieve the first byte back again, by swapping the accumulator with the secondary accumulator.
            ex af, af'
            ex de, hl   ; Put the image location back into de, and the video memory location back into hl.
            push de     ; Then since we are calling image_processByteUncompressed, preserve de
            ld e, a     ; Put the first image byte into e ...
            call image_processByteUncompressed ; ... then call image_processByteUncompressed to process this byte.
            ex af, af'
            ld e, a        ; Next swap the accumulator with the secondary accumulator to retrieve the second image byte, put this in e then .....
            call image_processByteUncompressed ; ... call image_processByteUncompressed to process the second image byte.
            pop de      ; Finally retrieve the memory counter value of de, since image_processByteUncompressed modifies both d and e.
            
        imageIfEndComp:
        
    
    ld a, l                       ; Loop while we have not reached SCREEN_MEMEND in hl yet.
    cp SCREEN_MEMEND_R            ; If there are any image errors, this loop may never end.
    jp nz, imageLoop_fillScreen
    ld a, h
    cp SCREEN_MEMEND_L
    jp nz, imageLoop_fillScreen
    
    pop bc
    pop de
ret





; Processes a single uncompressed byte. This is stored literally rather than using run-length
; encoding. This means that this method has to put all the literal bits onto the buffer.
;
; (well-behaved)
;
; Parameters:
;       e: The uncompressed byte to process
; Modifies:
;       b, c, d, hl
; Returns:
;       hl: This will now store the next video memory position to write to.
;
image_processByteUncompressed:    
    ld d, 8 ; Use d as a counter - eight to zero.
    
    imageLoop_processByteUncompressed: ; Loop 8 times:
        rlc e   ; Rotate e left by one, to get the next bit to add to the buffer.
        ld a, e ; Get e into the accumulator:
        and 1   ; Get it's first bit
        call image_processBit ; Process the first bit
        dec d   ; Decrement the counter variable
    jp nz, imageLoop_processByteUncompressed
    
ret





; Processes a single run-length encoded byte.
; The run length encoded byte is stored as such:
;  --> The most significant bit stores the colour
;  --> The least significant 7 bits store the number of times that colour is repeated.
;
; (well-behaved)
;
; Parameters:
;       e: The byte to process
; Modifies:
;       b, c, d, e, hl
; Returns:
;       hl: This will now store the next video memory position to write to.
;
image_processByteCompressed:
    ; b and c are the bit buffer and counter used in image_processBit.
    ; e is the byte that is requested.
    ; d is used as a counter.
    ld e, a
    and 127 ; Put the number of repeated colours (the least significant 7 bits) into d.
    ld d, a
    
    imageLoop_processByte:
        ld a, e
        and 0x80 ; Get the colour bit into a
        rlc a    ; Rotate it left so it becomes the least significant bit
        call image_processBit ; Process the bit
        dec d    ; Decrement the counter.
        
    ld a, d  ; Loop while we have not reached zero yet --> This loops <d> times.
    jp nz, imageLoop_processByte
    
ret




; Adds a single bit to the bit buffer b, if that has been filled, it is written
; to the video memory position specified by hl. The video memory position hl is then
; incremented with generic_incrementVideoPosition if something has been drawn.
;
; (well-behaved)
;
; Parameters:
;       b: The bit buffer
;       c: The number of bits written to the bit buffer
;       a: Zero or one: Should be the bit to write
; Modifies:
;       b, c, hl
; Returns:
;       hl: This will now store the next video memory position to write to.
;
image_processBit:
    ; b is the bit buffer - it stores the bits that have been written.
    ; c stores how many bits have been written to the bit buffer
    ; The bit to write should be specified in the accumulator - either 1 or zero.
    ; hl should store the current memory address.

    sla b   ; Shift bit buffer left
    or b    ; Add the new bit from the accumulator
    ld b, a ; Store back into the buffer
    inc c   ; Increment the write counter
    ld a, c
    cp 8
    ret nz  ; If the write counter is at 8 writes (i.e: full)
    
        ld c, 0 ; Reset the write counter
        ld (hl), b  ; Draw this byte to the video buffer
        call generic_incrementVideoPosition
ret
