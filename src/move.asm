; zx-spectrum-pong, (Wilso-Pong)
; Copyright (C) December 2017, Daniel Wilson
; 
; This program is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.
; 
; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
; 
; You should have received a copy of the GNU General Public License
; along with this program.  If not, see <https://www.gnu.org/licenses/>.



; This controls the ball's positioning, using a fixed
; point notation.




move_x: defb MOVE_INITIALX_L, MOVE_INITIALX_M
move_y: defb MOVE_INITIALY_L, MOVE_INITIALY_M

move_xSpeed: defb MOVE_INITIAL_XSPEED_L, MOVE_INITIAL_XSPEED_M
move_ySpeed: defb MOVE_INITIAL_YSPEED_L, MOVE_INITIAL_YSPEED_M



; Initialises the movement. This should be run before any movement is performed.
;
; (well-behaved)
;
; Parameters:
;       NONE
; Modifies:
;       hl
; Returns:
;       NONE
;
move_init:
    push de
    
    ld h, MOVE_INITIALX_M
    ld l, MOVE_INITIALX_L
    ld (move_x), hl
    
    ld h, MOVE_INITIALY_M
    ld l, MOVE_INITIALY_L
    ld (move_y), hl
    
    ld h, MOVE_INITIAL_XSPEED_M
    ld l, MOVE_INITIAL_XSPEED_L
    ld (move_xSpeed), hl
    
    ld h, MOVE_INITIAL_YSPEED_M
    ld l, MOVE_INITIAL_YSPEED_L
    ld (move_ySpeed), hl
    
    pop de
ret




; Runs one move step
;
; (well-behaved)
;
; Parameters:
;       NONE
; Modifies:
;       a, hl
; Returns:
;       NONE
;
move_step:
    push de ; Preserve de
    push bc
    
    call move_update
    
    call move_checkForHorizontalCollisions
    call move_checkForVerticalCollisions
    
    call move_setBallPos
    
    ;call move_addRandomSpeed
    
    pop bc
    pop de
ret








; Updates move_x and move_y from move_xSpeed and move_ySpeed
;
; (simple)
;
; Parameters:
;       NONE
; Modifies:
;       de, hl
; Returns:
;       NONE
; 
move_update:
    
    ld hl, (move_xSpeed) ; Get xSpeed into de
    ex de,hl
    ld hl, (move_x)      ; Get x position into hl
    add hl, de           ; Add xSpeed onto x position
    ld (move_x), hl      ; Store back into move_x
    
    ld hl, (move_ySpeed) ; Get ySpeed into de
    ex de,hl
    ld hl, (move_y)      ; Get y position into hl
    add hl, de           ; Add ySpeed onto y position
    ld (move_y), hl      ; Store back into move_y
    
ret



; Updates ball_x and ball_y from move_x and move_y
;
; (simple)
;
; Parameters:
;       NONE
; Modifies:
;       a, hl
; Returns:
;       NONE
; 
move_setBallPos:
    
    
    ; ------------ SIMPLER METHOD: SUPPOSEDLY SLIGHTLY SLOWER ----------
    
    ;ld hl, (move_x)    ; Get move_x into hl
    ;
    ;srl l
    ;srl l    ; Shift l right 4 bits
    ;srl l    ; this throws away the least significant 4 bits
    ;srl l
    ;
    ;sla h
    ;sla h    ; Shift h left 4 bits
    ;sla h    ; this throws away the most significant 4 bits
    ;sla h
    ;
    ;ld a, l  ; Add h and l
    ;add h
    ;
    ;ld (ball_x), a   ; Store into ball_x
    
    
    ; -- MORE COMPLEX BIT ROTATION METHOD: SUPPOSEDLY SLIGHTLY FASTER --
    
    
    ld hl, (move_x)    ; Get move_x into hl
    
    ld a, h
    and 15    ; Throw away the most significant 4 bits of h
    ld h, a
    
    ld a, l
    and 240   ; Throw aray the least significant 4 bits of l
    
    add h     ; Add h:
    ; we now have in the accumulator:  [4 msb of l][4 lsb of h]
    rrca
    rrca ; So rotate right 4 times
    rrca
    rrca
    ; we now have in the accumulator: [4 lsb of h][4 msb of l]
    
    ld (ball_x), a ; Store into ball x
    
    
    
    
    
    
    ld hl, (move_y)    ; Get move_y into hl
    
    ld a, h
    and 15    ; Throw away the most significant 4 bits of h
    ld h, a
    
    ld a, l
    and 240   ; Throw aray the least significant 4 bits of l
    
    add h     ; Add h:
    ; we now have in the accumulator:  [4 msb of l][4 lsb of h]
    rrca
    rrca ; So rotate right 4 times
    rrca
    rrca
    ; we now have in the accumulator: [4 lsb of h][4 msb of l]
    
    ld (ball_y), a ; Store into ball x
    
    
ret






; Checks for horizontal collisions, acts appropriately
;
; (simple)
;
; Parameters:
;       NONE
; Modifies:
;       de, hl, a
; Returns:
;       NONE
; 
move_checkForHorizontalCollisions:
    
    ld hl, (move_x)   ; Get move x
    ld de, MOVE_MINX  ; Get min x
    
    ld b, h  ; Preserve the old value of moveX - so it can be re-used for checking for right collisions.
    ld c, l
    
    sbc hl, de  ; Subtract:   moveX - minX
    
    jp p, moveIf_noLeftCollision  ; If moveX < minX: A left horizontal collision.
        
        ex de, hl
        ld hl, MOVE_MINX
        sbc hl, de
        ld (move_x), hl   ; Set move_x to new value taking into account the collision
        
        ld hl, (move_xSpeed)
        ex de, hl
        ld hl, 0              ; Invert the x speed
        sbc hl, de
        ld (move_xSpeed), hl
        
        call move_addRandomSpeed ; When a ball collides, add some random speed
        
        ld hl, (move_y)       ; Check if ball is within range, put this in gameRunning
        call move_isYPositionWininRangeOfLeftBat
        ld (main_gameRunning), a
        
        ret ; If a left collision has happened, a right collision cannot possibly have happened.
        
    moveIf_noLeftCollision:
    
    
    
    
    
    ld hl, MOVE_MAXX ; Get max x
                     ; Note: move x is still preserved in bc
                     
    sbc hl, bc ; Subtract    maxX - moveX
    
    jp p, moveIf_noRightCollision    ; If    moveX > maxX
        
        ld de, MOVE_MAXX
        add hl, de        ; Set move_x to a new value taking into account the collision
        ld (move_x), hl
        
        ld hl, (move_xSpeed)
        ex de, hl
        ld hl, 0              ; Invert the x speed
        sbc hl, de
        ld (move_xSpeed), hl
        
        call move_addRandomSpeed ; When a ball collides, add some random speed
        
        ld hl, (move_y)       ; Check if ball is within range, put this in gameRunning
        call move_isYPositionWininRangeOfRightBat
        ld (main_gameRunning), a
        
    moveIf_noRightCollision:
ret









; Checks for vertical collisions, acts appropriately
;
; (simple)
;
; Parameters:
;       NONE
; Modifies:
;       de, hl, a
; Returns:
;       NONE
; 
move_checkForVerticalCollisions:
    
    ld hl, (move_y)   ; Get move y
    ld de, MOVE_MINY  ; Get min y
    
    ld b, h  ; Preserve the old value of moveY - so it can be re-used for checking for bottom collisions.
    ld c, l
    
    sbc hl, de  ; Subtract:   moveY - minY
    
    jp p, moveIf_noTopCollision  ; If moveY < minY: A top vertical collision.
        ex de, hl
        ld hl, MOVE_MINY
        sbc hl, de
        ld (move_y), hl   ; Set move_y to new value taking into account the collision
        
        ld hl, (move_ySpeed)
        ex de, hl
        ld hl, 0              ; Invert the y speed
        sbc hl, de
        ld (move_ySpeed), hl
        
        call move_addRandomSpeed ; When a ball collides, add some random speed
        
        ret ; If a top collision has happened, a bottom collision cannot possibly have happened.
        
    moveIf_noTopCollision:
    
    
    
    
    
    ld hl, MOVE_MAXY ; Get max y
                     ; Note: move y is still preserved in bc
                     
    sbc hl, bc ; Subtract    maxY - moveY
    
    jp p, moveIf_noBottomCollision    ; If    moveY > maxY
        
        ld de, MOVE_MAXY
        add hl, de        ; Set move_y to a new value taking into account the collision
        ld (move_y), hl
        
        ld hl, (move_ySpeed)
        ex de, hl
        ld hl, 0              ; Invert the y speed
        sbc hl, de
        ld (move_ySpeed), hl
        
        call move_addRandomSpeed ; When a ball collides, add some random speed
        
    moveIf_noBottomCollision:
ret










; Checks if the y position stored in hl is within range of the left bat.
;
; (well-behaved)
;
; Parameters:
;       hl: Y position (fixed point fractional)
; Modifies:
;       a, hl
; Returns:
;       a: 1 If the position is in range, zero otherwise.
;
move_isYPositionWininRangeOfLeftBat:
    push de
    push bc
        ld a, (bat_leftPos)   ; Get bat left pos
        rrca
        rrca ; Rotate 4 times to swap over the most and 
        rrca ; least significant 4 bits.
        rrca
        
        ld d, a
        and 240  ; Put most significant 4 bits into e
        ld e, a
        
        ld a, d
        and 15   ; Put least significant 4 bits into e
        ld d, a
                    ; NOW:   hl = y position  AND  de = bat y pos,    so:
        ex de, hl
        ld bc, 128   ; Subtract 8 pixels from bat y position, to account for size of the ball.
        sbc hl, bc
        ex de, hl
        
        ld b, h ; Preserve the old value of hl   (old y position)
        ld c, l ; into bc to access it later.
        
        sbc hl, de    ; Do    (ball y position) - (bat y position)
        
        ; If (ball y position) <= (bat y position), it is out of range, so return.
        jp m, move_yPositionOutOfRangeOfLeftBat
        jp z, move_yPositionOutOfRangeOfLeftBat
        
        
        ld h, b   ; Get the original value of y back again
        ld l, c
        
        ld bc, MOVE_FRACTIONAL_LEFT_BAT_HEIGHT
        ex de, hl
        add hl, bc                             ; Add the bat's adjusted collision area height onto the bat's position (stored in de)
        ex de, hl
        
        sbc hl, de    ; Do    (ball y position) - (y position of furthest down extent of the bat's collision area)
        
        ; If (ball y position) >= (y position of furthest down extent of the bat's collision area)
        ; then it is out of range, so return.
        jp p, move_yPositionOutOfRangeOfLeftBat
        
        ; Otherwise, the ball must be within range, so:
        ld a, 1 ; Load accumulator with one
        
    pop bc ; And return.
    pop de
    ret
    
    move_yPositionOutOfRangeOfLeftBat:
        ld a, 0 ; If out of range, load accumulator with zero
        pop bc  ; then return.
        pop de
ret







; Checks if the y position stored in hl is within range of the right bat.
;
; (well-behaved)
;
; Parameters:
;       hl: Y position (fixed point fractional)
; Modifies:
;       a, hl
; Returns:
;       a: 1 If the position is in range, zero otherwise.
;
move_isYPositionWininRangeOfRightBat:
    push de
    push bc
        ld a, (bat_rightPos)   ; Get bat right pos
        rrca
        rrca ; Rotate 4 times to swap over the most and 
        rrca ; least significant 4 bits.
        rrca
        
        ld d, a
        and 240  ; Put most significant 4 bits into e
        ld e, a
        
        ld a, d
        and 15   ; Put least significant 4 bits into e
        ld d, a
                    ; NOW:   hl = y position  AND  de = bat y pos,    so:
        ex de, hl
        ld bc, 128   ; Subtract 8 pixels from bat y position, to account for size of the ball.
        sbc hl, bc
        ex de, hl
        
        ld b, h ; Preserve the old value of hl   (old y position)
        ld c, l ; into bc to access it later.
        
        sbc hl, de    ; Do    (ball y position) - (bat y position)
        
        ; If (ball y position) <= (bat y position), it is out of range, so return.
        jp m, move_yPositionOutOfRangeOfRightBat
        jp z, move_yPositionOutOfRangeOfRightBat
        
        
        ld h, b   ; Get the original value of y back again
        ld l, c
        
        ld bc, MOVE_FRACTIONAL_RIGHT_BAT_HEIGHT
        ex de, hl
        add hl, bc                             ; Add the bat's adjusted collision area height onto the bat's position (stored in de)
        ex de, hl
        
        sbc hl, de    ; Do    (ball y position) - (y position of furthest down extent of the bat's collision area)
        
        ; If (ball y position) >= (y position of furthest down extent of the bat's collision area)
        ; then it is out of range, so return.
        jp p, move_yPositionOutOfRangeOfLeftBat
        
        ; Otherwise, the ball must be within range, so:
        ld a, 1 ; Load accumulator with one
        
    pop bc ; And return.
    pop de
    ret
    
    move_yPositionOutOfRangeOfRightBat:
        ld a, 0 ; If out of range, load accumulator with zero
        pop bc  ; then return.
        pop de
ret










; Adds some random speed onto x and y speeds
;
; (simple)
;
; Parameters:
;       NONE
; Modifies:
;       a, hl, de
; Returns:
;       a: 1 If the position is in range, zero otherwise.
;
move_addRandomSpeed:
    ld a, r
    ld e, a
    
    ld hl, (move_ySpeed)
    call move_addRandomToValue
    ld (move_ySpeed), hl
    
    srl e
    srl e
    srl e
    
    ld hl, (move_xSpeed)
    call move_addRandomToValue
    ld (move_xSpeed), hl
    
ret







; Simulates a random offset to the value:
; A randomness value is given as e, only the 4 least significant bits are used:
; Bit 3, Bit 2, Bit 1, Bit 0,     where bit 0 is the least significant bit.
; The function does this to hl:
;   - If bit 3 is zero AND bit 2 is one: 
;         --> Bit 0, bit 1 and bit 2 are subtracted from hl
;   - Otherwise:
;         --> Bit 0, bit 1 and bit 2 are added onto hl
;
; (simple)
;
; Parameters:
;       hl: The value to add to
;       e:  The randomness to use, the least significant four bits will be used.
; Modifies:
;       a, hl, de
; Returns:
;       hl: The original value of hl with some randomness added
;
move_addRandomToValue:
    
    ld a, h   ; Get the first byte of hl
    and 0x80  ; Get the sign bit
    ld d, a   ; Store this in d
    
    ; Now:   d = 0x80 if hl is negative,   d = 0 if hl is positive 
    
    ld a, e   ; Get the randomness value
    and 12    ; Get bits 2 and 3
    cp 4      ; Compare to 4:   if (bit 3 is zero AND bit 2 is one) the z flag will be set
    
    ld a, d   ; Put d in the accumulator.
    jp nz, moveIf_addRandom1 ; Check if should invert:
        xor 0x80             ; If (bit 3 is zero AND bit 2 is one) then flip the most significant bit.
    moveIf_addRandom1:
    
    ; Now:
    ;   - a = 0x80 if: (hl is negative AND should not invert) OR (hl is positive AND should invert)
    ;   - a = 0    if: (hl is positive AND should invert)     OR (hl is negative AND should not invert)
    
    cp 0
    jp z, moveIf_addRandom2 ; If a = 0
        ld d, e ; Back up randomness into d
        ld a, e ; Get randomness into a
        and 7   ; Throw away the most significant 5 bits
        ld e, a ; Store this back into e
        ld a, d ; Get the old randomness value into the accumulator
        ld d, 0 ; Then store zero into d
        sbc hl, de ; Do subtraction
    jp moveElse_addRandom2
    moveIf_addRandom2:      ; Else a = 0x80
        ld d, e ; Back up randomness into d
        ld a, e ; Get randomness into a
        and 7   ; Throw away the most significant 5 bits
        ld e, a ; Store this back into e
        ld a, d ; Get the old randomness value into the accumulator
        ld d, 0 ; Then store zero into d
        add hl, de ; Do addition
    moveElse_addRandom2:
    
    ld e, a
    ; If d = 0x80, the randomness will be added
    ; if d = 0, the randomness will be subtracted
    
ret
