; zx-spectrum-pong, (Wilso-Pong)
; Copyright (C) December 2017, Daniel Wilson
; 
; This program is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.
; 
; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
; 
; You should have received a copy of the GNU General Public License
; along with this program.  If not, see <https://www.gnu.org/licenses/>.




; This code is concerned with drawing the ball in the correct
; position.




ball_x: defw BALL_INITIALX
ball_y: defw BALL_INITIALY


ball_oldX: defw BALL_INITIALX
ball_oldY: defw BALL_INITIALY



ball_firstCol:  defb   0, -128, -64, -32, -16, -8, -4, -2
ball_secondCol: defb  -1,  127,  63,  31,  15,  7,  3,  1 






; Draws the initial position of the ball, and resets all of the ball variables.
; This MUST be run before any of the ball system is used.
;
; (well-behaved)
;
; Parameters:
;       NONE
; Modifies:
;       hl
; Returns:
;       NONE
;
ball_init:
    ld hl, ball_x
    ld (hl), BALL_INITIALX
    ld hl, ball_y
    ld (hl), BALL_INITIALY
    ld hl, ball_oldX
    ld (hl), BALL_INITIALX
    ld hl, ball_oldY
    ld (hl), BALL_INITIALY
    
    call ball_draw
ret





; Draws the initial position of the ball.
;
; (well behaved)
;
; Parameters:
;       NONE
; Modifies:
;       a, hl
; Returns:
;       NONE
; 
ball_draw:
    push bc
    push de
    ; b  X offset (0-8)    - then char to use
    ; c  x column (0-32)
    ; e  y start / current
    ; d  y end
    
    ld a, (ball_x) ; Get ball x position
    ld c, a ; Put this into c
    srl c
    srl c ; Shift c (x-col) three times to the right (divide it by 8 to get column number)
    srl c
    
    and 7   ; Get first three bits, this is x offset
    
    ld hl, ball_firstCol ; Put mem address of firstCol into hl
    ld d, 0              ; load de as a
    ld e, a
    add hl, de           ; Add de (the offset) onto hl
    ld b, (hl)           ; Store this into b
    
    
    ld a, (ball_y) ; Get ball actual y position
    ld e, a        ; Put this into e
    
    add 8   ; Add 8 - the end y position
    ld d, a ; Put this into d
    
    ballLoop_drawInitial:   ; Loop:
    
        call generic_getY ; Get memory location from y
        ld l
        add c      ; Add x position onto it, this is position of first col.
        ld l, a
        ld a, b    ; Get b (the char to use) into a
        ld (hl), a ; Set the left col memory location to a
        
        cp 0  ; Only draw the second column if not about to draw a zero.
        jp z, ballIf_drawInitial
            cpl        ; Invert a, to get the character to use for the second col
            inc l      ; Increment l, to go one column to the right
            ld (hl), a ; Set the right col memory location to a
        ballIf_drawInitial:
        
        inc e ; Increment e
    
    ld a, e   ; do-while e (current y position) has not yet reached d (the end y position).
    cp d
    jp nz, ballLoop_drawInitial
    
    
    pop de
    pop bc
ret






; Overwrites the old position of the ball
;
; (well behaved)
;
; Parameters:
;       NONE
; Modifies:
;       a, hl
; Returns:
;       NONE
; 
ball_overwritePrevious:
    push bc
    push de
    ; b  x offset
    ; c  x column (0-32)
    ; e  y start / current
    ; d  y end
    
    ld a, (ball_oldX) ; Get ball x position
    ld c, a ; Put this into c
    
    and 7
    ld b, a
    
    srl c
    srl c ; Shift c (x-col) three times to the right (divide it by 8 to get column number)
    srl c
    
    
    
    
    ld a, (ball_oldY) ; Get ball actual y position
    ld e, a        ; Put this into e
    
    add 8   ; Add 8 - the end y position
    ld d, a ; Put this into d
    
    ballLoop_overwritePrevious:   ; Loop:
    
        call generic_getY ; Get memory location from y
        ld l
        add c      ; Add x position onto it, this is position of first col.
        ld l, a
        
        ld (hl), 0xff ; Set the first col to blank
        
        ld a, b
        cp 0  ; Only draw the second column if x offset is not zero
        jp z, ballIf_overwritePrevious
            inc l         ; Increment l, to go one column to the right
            ld (hl), 0xff ; Set the right col memory location to blank
        ballIf_overwritePrevious:
        
        inc e ; Increment e
    
    ld a, e   ; do-while e (current y position) has not yet reached d (the end y position).
    cp d
    jp nz, ballLoop_overwritePrevious
    
    
    pop de
    pop bc
ret








ball_redraw:
    call ball_overwritePrevious
    
    call ball_draw
    
    ld a, (ball_x)
    ld (ball_oldX), a
    
    ld a, (ball_y)
    ld (ball_oldY), a
    
ret






