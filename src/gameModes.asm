; zx-spectrum-pong, (Wilso-Pong)
; Copyright (C) December 2017, Daniel Wilson
; 
; This program is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.
; 
; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
; 
; You should have received a copy of the GNU General Public License
; along with this program.  If not, see <https://www.gnu.org/licenses/>.






gameModes_dual:

    ld e, 255                 ; Fill the screen with the colour black
    call generic_fillScreen

    
    call bat_init
    call ball_init    ; Reset/initiate the bat, ball and movement
    call move_init



    gameModesLoop_runStandard:
        
        ld a, 0xfb      ; Set address to 0xfb
        in a, (0xfe)    ; Read this address from input port 0xfe --> The keyboard input port
        and 2           ; Get bit representing W, 
        jp nz, gameModes_dual_wNotPressed
            ld e, 2
            call bat_moveLeftUp
        gameModes_dual_wNotPressed:
        
        
        ld a, 0xfd      ; Set address to 0xfd
        in a, (0xfe)    ; Read this address from input port 0xfe --> The keyboard input port
        and 2           ; Get bit representing S, 
        jp nz, gameModes_dual_sNotPressed
            ld e, 2
            call bat_moveLeftDown
        gameModes_dual_sNotPressed:
        
        
        ld a, 0xdf      ; Set address to 0xdf
        in a, (0xfe)    ; Read this address from input port 0xfe --> The keyboard input port
        and 2           ; Get bit representing O, 
        jp nz, gameModes_dual_oNotPressed
            ld e, 2
            call bat_moveRightUp
        gameModes_dual_oNotPressed:
        
        
        ld a, 0xbf      ; Set address to 0xbf
        in a, (0xfe)    ; Read this address from input port 0xfe --> The keyboard input port
        and 2           ; Get bit representing L, 
        jp nz, gameModes_dual_lNotPressed
            ld e, 2
            call bat_moveRightDown
        gameModes_dual_lNotPressed:
        
        
        
        
        call move_step
        call ball_redraw
        
        
        ld a, (main_gameRunning)
        cp 0
        ret z ; End game if main_gameRunning is zero
        
        
        
        ;ld e, 50
        ;call generic_waitForFrames
        call generic_waitForNextFrame
        

    jp gameModesLoop_runStandard

ret








gameModes_simple:

    ld e, 255                 ; Fill the screen with the colour black
    call generic_fillScreen

    call bat_init
    call ball_init    ; Reset/initiate the bat, ball and movement
    call move_init

    gameModesLoop_runSimple:
        
        ld a, 0xfb      ; Set address to 0xfb
        in a, (0xfe)    ; Read this address from input port 0xfe --> The keyboard input port
        
        ld b, a
        and 2           ; Get bit representing W, 
        jp nz, gameModes_simple_wNotPressed
            ld e, 2
            call bat_moveLeftUp
            call bat_moveRightUp
        gameModes_simple_wNotPressed:
        
        ld a, 0xfd      ; Set address to 0xfd
        in a, (0xfe)    ; Read this address from input port 0xfe --> The keyboard input port
        
        ld b, a
        and 2           ; Get bit representing S, 
        jp nz, gameModes_simple_sNotPressed
            ld e, 2
            call bat_moveLeftDown
            call bat_moveRightDown
        gameModes_simple_sNotPressed:
        
        call move_step
        call ball_redraw
        
        ld a, (main_gameRunning)
        cp 0
        ret z ; End game if main_gameRunning is zero
        
        call generic_waitForNextFrame
        
    jp gameModesLoop_runSimple

ret
