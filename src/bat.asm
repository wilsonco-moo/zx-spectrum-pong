; zx-spectrum-pong, (Wilso-Pong)
; Copyright (C) December 2017, Daniel Wilson
; 
; This program is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.
; 
; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
; 
; You should have received a copy of the GNU General Public License
; along with this program.  If not, see <https://www.gnu.org/licenses/>.







bat_leftPos:  defw BAT_LEFT_INITIAL_POSITION
bat_rightPos: defw BAT_RIGHT_INITIAL_POSITION

bat_leftPosOld:  defw BAT_LEFT_INITIAL_POSITION
bat_rightPosOld: defw BAT_RIGHT_INITIAL_POSITION







; Draws the bats' initial locations.
; NOTE: This function is very long, and contains repeated elements.
; This should be optimised for code size, not speed.
; This MUST be run before any bat stuff is used.
;
; (well behaved)
;
; Parameters:
;       NONE
; Modifies:
;       a, hl
; Returns:
;       NONE
; 
 bat_init:
    push de ; Preserve registers b, c, d, e
    push bc
    
    
    ld hl, bat_leftPos                     ; Set bat initial positions.
    ld (hl), BAT_LEFT_INITIAL_POSITION
    ld hl, bat_rightPos
    ld (hl), BAT_RIGHT_INITIAL_POSITION
    ld hl, bat_leftPosOld
    ld (hl), BAT_LEFT_INITIAL_POSITION
    ld hl, bat_rightPosOld
    ld (hl), BAT_RIGHT_INITIAL_POSITION
    
    
    
    ; Draw top-left background space
    
    ld b, BAT_LEFT_BACKGROUND_CHAR
    ld c, BAT_LEFT_X
    ld e, 0                 ; Start at y=0
    ld a, (bat_leftPos)
    ld d, a                 ; End at y=bat_leftPos
    call generic_drawVLine
    
    ; Draw left bat
    
    ld b, BAT_LEFT_CHAR
    ld a, d                 ; Start at previous end position (bat_leftPos)
    add BAT_LEFT_HEIGHT     ; End at bat_leftPos + SCREEN_HEIGHT
    ld d, a
    call generic_drawVLine
    
    ; Draw bottom-left background space
    
    ld b, BAT_LEFT_BACKGROUND_CHAR     ; Start at previous end position (bat_leftPos + SCREEN_HEIGHT)
    ld d, SCREEN_HEIGHT                ; End at screen height
    call generic_drawVLine
    
    
    ; Draw top-right background space
    
    ld b, BAT_RIGHT_BACKGROUND_CHAR
    ld c, BAT_RIGHT_X
    ld e, 0                 ; Start at y=0
    ld a, (bat_rightPos)
    ld d, a                 ; End at y=bat_rightPos
    call generic_drawVLine
    
    ; Draw right bat
    
    ld b, BAT_RIGHT_CHAR
    ld a, d                  ; Start at previous end position (bat_rightPos)
    add BAT_RIGHT_HEIGHT     ; End at bat_rightPos + SCREEN_HEIGHT
    ld d, a
    call generic_drawVLine
    
    ; Draw bottom-right background space
    
    ld b, BAT_RIGHT_BACKGROUND_CHAR     ; Start at previous end position (bat_rightPos + SCREEN_HEIGHT)
    ld d, SCREEN_HEIGHT                 ; End at screen height
    call generic_drawVLine
    
    
    
    
    pop bc
    pop de
ret






; Moves the left bat up by the specified amount,
; and does checks to ensure that the bat does not fall off the top.
;
; (well-behaved)
;
; Parameters:
;       e: The amount to move the bat up by
; Modifies:
;       a, hl
; Returns:
;       NONE
; 
bat_moveLeftUp:
    ; --------------- DO THE POSITIONAL CALCULATION ----------------------
    ld a, (bat_leftPos)      ; Get the current left position
    ld h, a                  ; Preserve it into h
    sub e                    ; Subtract e from it. It is not an invalid value IF it is less than zero, but NOT if it is less than or equal to -64 --> Signed byte overflow.
    and 0xc0                 ; Get the first two bits
    cp 0xc0                  ; The value is invalid if both these bits are ONE --> Small negative value
    jp z, batIf_moveLeftUp   ; If both are zero, jump to else
        ld a, h              ;  IF NOT INVALID:
        sub e                ;  Subtract e from position
        ld (bat_leftPos), a  ;  Store it
        ld l, e              ; Set distance actually moved to the distance to move up by, specified in e.
        ld h, a              ; Preserve the new bat position.
    jp batIfEnd_moveLeftUp   ; Otherwise:
    batIf_moveLeftUp:        ;  IF INVALID:
        ld a, 0              ;  Just set position to zero.
        ld (bat_leftPos), a  ;  Store it.
        ld l, h              ; Set distance actually moved (l) to the old value of bat_leftPos
        ld h, 0              ; Preserve the new bat position
    batIfEnd_moveLeftUp:
    
    ; ----------------- DO THE DRAWING ----------------------------------
    
    
    ; l is now the distance actually moved
    ; h is now the new position
    ld a, l
    cp 0
    ret z ; If has not actually moved at all, do nothing else (i.e: Don't draw anything).
    
    push bc ; Preserve b,c,d,e
    push de

        ld b, BAT_LEFT_CHAR ; Use left char
        ld c, BAT_LEFT_X    ; Use left x
        ld a, h
        ld e, a ; Use h as start y
        add l
        ld d, a ; Use h+l as end y
        push hl
            call generic_drawVLine  ; Draw vLine
        pop hl ; Preserve h and l
        
        
        ld b, BAT_LEFT_BACKGROUND_CHAR
        ld a, h
        add BAT_LEFT_HEIGHT
        ld e, a ; Use h + height as start y
        add l
        ld d, a ; Use h+l + height as end y
        call generic_drawVLine  ; Draw vLine
        ; No need to preserve hl - it isn't used again.
    
    pop de
    pop bc
ret






; Moves the left bat down by the specified amount.
;
; (well-behaved)
;
; Parameters:
;       e: The amount to move the bat down by
; Modifies:
;       a, hl
; Returns:
;       NONE
; 
bat_moveLeftDown:
    ld a, (bat_leftPos)      ; Get the current left position
    ld h, a                  ; Preserve it into h
    add BAT_LEFT_HEIGHT      ; Add the bat height, so that invalid values are in the correct range
    add e                    ; Add e to it. It is not an invalid value IF it is less than zero, but NOT if it is less than or equal to -64 --> Signed byte overflow.
    and 0xc0                 ; Get the first two bits
    cp 0xc0                  ; The value is invalid if both these bits are ONE --> Small negative value
    jp z, batIf_moveLeftDown ; If both are zero, jump to else
        ld a, h              ;  IF NOT INVALID:
        add e                ;  Subtract e from position
        ld (bat_leftPos), a  ;  Store it
        ld l, e              ; Set l to e - the distance specified
        ; Don't update h to store the new position: when moving down, we start drawing from the old position.
    jp batEndIf_moveLeftDown ; Otherwise:
    batIf_moveLeftDown:      ;  IF INVALID:
        ld a, BAT_LEFT_MAXPOS;  Just set position to maximum.
        ld (bat_leftPos), a  ;  Store it.
        
        ld a, h                  ; Here, to get the actual distance moved: get h into acc
        xor 0x80                 ; Flip the sign bit, so we can do simulated unsigned arithmetic
        sub BAT_LEFT_MAXPOS_INV  ; Subtract the left maxpos (with sign bit also inverted)
        neg                      ; This will produce 0-(actual distance moved),   so invert it
        ld l, a                  ; Finally put this into l
        
    batEndIf_moveLeftDown:
    
    ; ----------------- DO THE DRAWING ----------------------------------
    
    ; l is now the distance actually moved downwards
    ; h is now the old position
    ld a, l
    cp 0
    ret z ; If has not actually moved at all, do nothing else (i.e: Don't draw anything).
    
    push bc ; Preserve b,c,d,e
    push de

        ld b, BAT_LEFT_BACKGROUND_CHAR ; Use left char
        ld c, BAT_LEFT_X    ; Use left x
        ld a, h
        ld e, a ; Use h as start y
        add l
        ld d, a ; Use h+l as end y
        push hl
            call generic_drawVLine  ; Draw vLine
        pop hl ; Preserve h and l
        
        
        ld b, BAT_LEFT_CHAR
        ld a, h
        add BAT_LEFT_HEIGHT
        ld e, a ; Use h + height as start y
        add l
        ld d, a ; Use h+l + height as end y
        call generic_drawVLine  ; Draw vLine
        ; No need to preserve hl - it isn't used again.
        
    pop de
    pop bc
ret






; Moves the right bat up by the specified amount,
; and does checks to ensure that the bat does not fall off the top.
;
; (well-behaved)
;
; Parameters:
;       e: The amount to move the bat up by
; Modifies:
;       a, hl
; Returns:
;       NONE
; 
bat_moveRightUp:
    ; --------------- DO THE POSITIONAL CALCULATION ----------------------
    ld a, (bat_rightPos)     ; Get the current left position
    ld h, a                  ; Preserve it into h
    sub e                    ; Subtract e from it. It is not an invalid value IF it is less than zero, but NOT if it is less than or equal to -64 --> Signed byte overflow.
    and 0xc0                 ; Get the first two bits
    cp 0xc0                  ; The value is invalid if both these bits are ONE --> Small negative value
    jp z, batIf_moveRightUp  ; If both are zero, jump to else
        ld a, h              ;  IF NOT INVALID:
        sub e                ;  Subtract e from position
        ld (bat_rightPos), a ;  Store it
        ld l, e              ; Set distance actually moved to the distance to move up by, specified in e.
        ld h, a              ; Preserve the new bat position.
    jp batIfEnd_moveRightUp  ; Otherwise:
    batIf_moveRightUp:       ;  IF INVALID:
        ld a, 0              ;  Just set position to zero.
        ld (bat_rightPos), a ;  Store it.
        ld l, h              ; Set distance actually moved (l) to the old value of bat_leftPos
        ld h, 0              ; Preserve the new bat position
    batIfEnd_moveRightUp:
    
    ; ----------------- DO THE DRAWING ----------------------------------
    
    
    ; l is now the distance actually moved
    ; h is now the new position
    ld a, l
    cp 0
    ret z ; If has not actually moved at all, do nothing else (i.e: Don't draw anything).
    
    push bc ; Preserve b,c,d,e
    push de

        ld b, BAT_RIGHT_CHAR ; Use left char
        ld c, BAT_RIGHT_X    ; Use left x
        ld a, h
        ld e, a ; Use h as start y
        add l
        ld d, a ; Use h+l as end y
        push hl
            call generic_drawVLine  ; Draw vLine
        pop hl ; Preserve h and l
        
        
        ld b, BAT_RIGHT_BACKGROUND_CHAR
        ld a, h
        add BAT_RIGHT_HEIGHT
        ld e, a ; Use h + height as start y
        add l
        ld d, a ; Use h+l + height as end y
        call generic_drawVLine  ; Draw vLine
        ; No need to preserve hl - it isn't used again.
        
    pop de
    pop bc
ret






; Moves the right bat down by the specified amount.
;
; (well-behaved)
;
; Parameters:
;       e: The amount to move the bat down by
; Modifies:
;       a, hl
; Returns:
;       NONE
; 
bat_moveRightDown:
    ld a, (bat_rightPos)      ; Get the current left position
    ld h, a                   ; Preserve it into h
    add BAT_RIGHT_HEIGHT      ; Add the bat height, so that invalid values are in the correct range
    add e                     ; Add e to it. It is not an invalid value IF it is less than zero, but NOT if it is less than or equal to -64 --> Signed byte overflow.
    and 0xc0                  ; Get the first two bits
    cp 0xc0                   ; The value is invalid if both these bits are ONE --> Small negative value
    jp z, batIf_moveRightDown ; If both are zero, jump to else
        ld a, h               ;  IF NOT INVALID:
        add e                 ;  Subtract e from position
        ld (bat_rightPos), a  ;  Store it
        ld l, e               ; Set l to e - the distance specified
        ; Don't update h to store the new position: when moving down, we start drawing from the old position.
    jp batEndIf_moveRightDown ; Otherwise:
    batIf_moveRightDown:      ;  IF INVALID:
        ld a, BAT_RIGHT_MAXPOS;  Just set position to maximum.
        ld (bat_rightPos), a  ;  Store it.
        
        ld a, h                  ; Here, to get the actual distance moved: get h into acc
        xor 0x80                 ; Flip the sign bit, so we can do simulated unsigned arithmetic
        sub BAT_RIGHT_MAXPOS_INV ; Subtract the right maxpos (with sign bit also inverted)
        neg                      ; This will produce 0-(actual distance moved),   so invert it
        ld l, a                  ; Finally put this into l
        
    batEndIf_moveRightDown:
    
    ; ----------------- DO THE DRAWING ----------------------------------
    
    ; l is now the distance actually moved downwards
    ; h is now the old position
    ld a, l
    cp 0
    ret z ; If has not actually moved at all, do nothing else (i.e: Don't draw anything).
    
    push bc ; Preserve b,c,d,e
    push de

        ld b, BAT_RIGHT_BACKGROUND_CHAR ; Use left char
        ld c, BAT_RIGHT_X    ; Use left x
        ld a, h
        ld e, a ; Use h as start y
        add l
        ld d, a ; Use h+l as end y
        push hl
            call generic_drawVLine  ; Draw vLine
        pop hl ; Preserve h and l
        
        
        ld b, BAT_RIGHT_CHAR
        ld a, h
        add BAT_RIGHT_HEIGHT
        ld e, a ; Use h + height as start y
        add l
        ld d, a ; Use h+l + height as end y
        call generic_drawVLine  ; Draw vLine
        ; No need to preserve hl - it isn't used again.
    
    pop de
    pop bc
ret
