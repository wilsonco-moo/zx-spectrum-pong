; zx-spectrum-pong, (Wilso-Pong)
; Copyright (C) December 2017, Daniel Wilson
; 
; This program is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.
; 
; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
; 
; You should have received a copy of the GNU General Public License
; along with this program.  If not, see <https://www.gnu.org/licenses/>.



; REGISTER USE POLICY
; 
; There are two kinds of function:
;
; Well-behaved function
; *********************
;  - This uses the system stack to preserve the values of any registers
;    that is uses, except for a and optionally hl
;  - The accumulator is the only register that a well-behaved method
;    does not need to preserve. It can preserve it, but the documentation
;    should specify this.
;  - Well-behaved methods should take their parameters from registers,
;    the system stack, or a memory location. This should be specified in
;    it's associated documentation.
;  - The return value can also be a register, memory location or the
;    system stack. This should be specified in the documentation.
;
; Simple function
; ***************
;  - This is allowed to leave registers with a different value than they
;    started with.
;  - Any registers modified in the method body should be specified in the
;    associated documentation.
;  - The function should take it's parameters from registers, and use
;    registers for the return value. The system stack should not be used,
;    as simple methods are intended to execute quickly.
;
;
; Further register use info
; *************************
;
; Any simple function can be automatically expected to overwrite register a,
; as this is the accumulator. Almost all simple functions can be automatially
; expected to overwrite h,l   (hl) - the 16-bit address register. As such,
; a, h and l should be used for temporary values in functions.
;  -- This should be specified in the documentation. ^
;
; Return values of functions should be a,h,l in preference to other registers,
; as these are special registers, and generally then load operations would not
; be needed to move values around in the code calling the function.
; The register(s) used should be specified in documentation.
;
; Use of other registers
; **********************
;
; The other general purpose registers are in the order:
; a, b, c, d, e
; These should be used as function parameters, starting from the end of the list.
; For example, if a function takes one parameter, it should use register e.
; If a function takes two parameters, it should use registers d and e.
;    .. etc.
;
; If the function needs any other temporary values than a, h, l, it should 
; use last registers before it's parameters. For example, if a function needs one
; temporary value, and it takes one parameter, it should use e as it's parameter,
; and use d as the temporary value. All of this should be specified in it's documentation.
;
;


; ------------------------------ screen ----------------------------

SCREEN_WIDTH:       equ    256
SCREEN_HEIGHT:      equ    192
SCREEN_AREA:        equ    SCREEN_WIDTH * SCREEN_HEIGHT



SCREEN_BYTES:       equ    SCREEN_AREA / 8
SCREEN_BYTES_R:     equ    SCREEN_BYTES & 0x00ff
SCREEN_BYTES_L:     equ    (SCREEN_BYTES & 0xff00) >> 8

SCREEN_MEMSTART:    equ    16384
SCREEN_MEMSTART_R:  equ    SCREEN_MEMSTART & 0x00ff
SCREEN_MEMSTART_L:  equ    (SCREEN_MEMSTART & 0xff00) >> 8

SCREEN_MEMEND:      equ    SCREEN_MEMSTART + SCREEN_BYTES
SCREEN_MEMEND_R:    equ    SCREEN_MEMEND & 0x00ff
SCREEN_MEMEND_L:    equ    (SCREEN_MEMEND & 0xff00) >> 8



SCREEN_COLS:        equ    SCREEN_WIDTH/8
SCREEN_ROWS:        equ    SCREEN_HEIGHT/8



; ------------------------------ bat ----------------------------



BAT_LEFT_HEIGHT:  equ 48
BAT_RIGHT_HEIGHT: equ 48

BAT_LEFT_CHAR:             equ @1 11100000
BAT_LEFT_BACKGROUND_CHAR:  equ @1 10111111

BAT_RIGHT_CHAR:            equ @1 00000111
BAT_RIGHT_BACKGROUND_CHAR: equ @1 11111101

BAT_LEFT_X:  equ 0
BAT_RIGHT_X: equ 31

BAT_LEFT_MINPOS:     equ 0
BAT_LEFT_MAXPOS:     equ SCREEN_HEIGHT - BAT_LEFT_HEIGHT
BAT_LEFT_MAXPOS_INV: equ BAT_LEFT_MAXPOS ^ 0x80

BAT_RIGHT_MINPOS:     equ 0
BAT_RIGHT_MAXPOS:     equ SCREEN_HEIGHT - BAT_RIGHT_HEIGHT
BAT_RIGHT_MAXPOS_INV: equ BAT_LEFT_MAXPOS ^ 0x80

BAT_LEFT_INITIAL_POSITION:  equ BAT_LEFT_MAXPOS/2  ; Put both bats in the middle to start with
BAT_RIGHT_INITIAL_POSITION: equ BAT_LEFT_MAXPOS/2


; -------------------------- BALL --------------------

; NOTE: THE BALL'S SIZE IS NOT A DEFINED CONSTANT. IT IS HARD-CODED
; AS 8, AND CANNOT CHANGE. THE IMPLEMENTATION OF DRAWING THE BALL DEPENDS
; ON THE BALL BEING THE SAME SIZE OF EACH MEMORY BYTE, SO THE SIZE IS
; A LIMITATION OF THE MEMORY SYSTEM, AND MUST NEVER BE ATTEMPTED TO CHANGE.

BALL_MINX:  equ   (BAT_LEFT_X+1)*8
BALL_MINY:  equ   0

BALL_MAXX:  equ   (BAT_RIGHT_X-1)*8
BALL_MAXY:  equ   SCREEN_HEIGHT-8

BALL_INITIALX:   equ (BALL_MINX + BALL_MAXX)/2 ; put ball initially at the centre of the screen
BALL_INITIALY:   equ (BALL_MINY + BALL_MAXY)/2



; -------------------------- MOVEMENT SPECIFIC CONSTANTS --------------


MOVE_INITIAL_XSPEED:    equ  -14
MOVE_INITIAL_YSPEED:    equ  -15


MOVE_INITIALX_M:   equ  (BALL_INITIALX >> 4)&255
MOVE_INITIALX_L:   equ  (BALL_INITIALX << 4)&255

MOVE_INITIALY_M:   equ  (BALL_INITIALY >> 4)&255
MOVE_INITIALY_L:   equ  (BALL_INITIALY << 4)&255





MOVE_INITIAL_XSPEED_M:  equ  (MOVE_INITIAL_XSPEED & 65280) >> 8
MOVE_INITIAL_XSPEED_L:  equ  MOVE_INITIAL_XSPEED & 255

MOVE_INITIAL_YSPEED_M:  equ  (MOVE_INITIAL_YSPEED & 65280) >> 8
MOVE_INITIAL_YSPEED_L:  equ  MOVE_INITIAL_YSPEED & 255


MOVE_MINX:     equ  BALL_MINX << 4
MOVE_MAXX:     equ  BALL_MAXX << 4

MOVE_MINY:     equ  BALL_MINY << 4
MOVE_MAXY:     equ  BALL_MAXY << 4


MOVE_MINX_M:   equ  (BALL_MINX >> 4)&255
MOVE_MINX_L:   equ  (BALL_MINX << 4)&255
MOVE_MAXX_M:   equ  (BALL_MAXX >> 4)&255
MOVE_MAXX_L:   equ  (BALL_MAXX << 4)&255

MOVE_MINY_M:   equ  (BALL_MINY >> 4)&255
MOVE_MINY_L:   equ  (BALL_MINY << 4)&255
MOVE_MAXY_M:   equ  (BALL_MAXY >> 4)&255
MOVE_MAXY_L:   equ  (BALL_MAXY << 4)&255


MOVE_FRACTIONAL_LEFT_BAT_HEIGHT:     equ      (BAT_LEFT_HEIGHT + 8) << 4   ; This is the height of the two bats' collision areas in fractional 16-bit format,
MOVE_FRACTIONAL_RIGHT_BAT_HEIGHT:    equ     (BAT_RIGHT_HEIGHT + 8) << 4   ; (8 is added to adjust for the height of the ball).
