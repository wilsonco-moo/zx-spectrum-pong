; zx-spectrum-pong, (Wilso-Pong)
; Copyright (C) December 2017, Daniel Wilson
; 
; This program is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.
; 
; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
; 
; You should have received a copy of the GNU General Public License
; along with this program.  If not, see <https://www.gnu.org/licenses/>.



;
; This file contains a series of functions for doing
; generic things, like clearing the screen, and converting
; between "Crazy Sir Clive's incoherent coordinate creation"
; and sensible coordinate values.





; Function to fill screen.
;
; (simple)
;
; Parameters:
;       e: Char to fill screen with
; Modifies:
;       a, hl
; Returns:
;       NONE
;
generic_fillScreen:

    ld hl, SCREEN_MEMSTART         ; Put memstart into hl
    genericLoop_fillScreen:        ; Loop
        ld (hl), e                 ; Put 0x55 into mem location
        inc hl                     ; Increment hl
    ld a, l                        ; Get the right byte
    cp SCREEN_MEMEND_R             ; If it is not yet the memory end byte
    jp nz, genericLoop_fillScreen ; Loop again
    ld a, h                        ; Get the left byte of hl
    cp SCREEN_MEMEND_L             ; If it is not yet the memory end byte
    jp nz, genericLoop_fillScreen ; Loop again

ret




; Function to get the memory address of the specified
; y-line on the screen.
;
; (simple)
;
; Parameters:
;       e: Y position on the screen (unsigned).
; Modifies:
;       a, hl
; Returns:
;       hl: The memory address.
generic_getY:
    ld h, SCREEN_MEMSTART_L ; Load SCREEN_MEMSTART_L into left byte.
                            ; Right byte of SCREEN_MEMSTART is zero, and is set later.
    ld a, e ; Get param -> acc
    and 7   ; Get least significant 7 bits
    add h   ; Put them into three least significant bits of left byte
    ld h, a ; Put back into h.
    
    ld a, e ; Get param again
    and 192 ; Get 2 most significant bits
    srl a   
    srl a   ; Shift right three times
    srl a
    add h   ; Add this onto left byte
    ld h, a
    
    ld a, e ; Get param for the final time
    and 56  ; Get bits 3,4,5
    sla a   ; Shift left twice
    sla a   ; ...
    ld l, a ; Make this the right byte. Everything else is zero.
    
ret




; Waits until another frame happens, by waiting for the frame counter
; to change from the value it was when this function is called.
;
; (simple)
;
; Parameters:
;       NONE
; Modifies:
;       a,e,hl
; Returns:
;       NONE
; 
generic_waitForNextFrame:
    ld hl, 23672
    ld e, (hl)
    genericLoop_waitForNextFrame:
        ld a, (hl)
    cp e
    jp z, genericLoop_waitForNextFrame
ret




; Waits the number of frames specified by e
;
; (well-behaved)
;
; Parameters:
;       e: Number of frames to wait.
; Modifies:
;       a, hl
; Returns:
;       NONE
; 
generic_waitForFrames:
    push bc
    ld b, e
    genericLoop_waitForTime:
        call generic_waitForNextFrame
        dec b
    ld a, b
    jp nz, genericLoop_waitForTime
    
    pop bc
ret




; Draws a vertical line of the specfied character, from the specified
; starting location, to the specified ending location.
;
; (simple)
;
; Parameters:
;       b: The character to use
;       c: The x coordinate
;       d: The end y coordinate
;       e: The start y coordinate. After the function completes, this will equal the end coordinate.
; Modifies:
;       a, hl, e
; Returns:
;       NONE
generic_drawVLine:

    genericLoop_drawVLine:     ; Loop while:
    ld a, e
    cp d                        ; e does not equal d (start has not yet reached end)
    ret z                       ; Return at end
        
        call generic_getY       ; Get y memory position of current y position (e)
        ld a, l
        add c                   ; Add the x coordinate onto it (c)
        ld l, a
        
        ld (hl), b              ; Set this memory location to the character (b)
        
        inc e                   ; Increment y position (e)
        
    jp genericLoop_drawVLine

; ret   ; Return statement not needed here, return statement is inside function loop.





; INFORMATION ABOUT BIT POSITIONING FOR generic_getY
;
;Y:    7 6 5 4 3 2 1 0
;
;      - - - - - - - - - - - - - - - -
;
;
;Put 0 1 2 in left byte
;      - - - - - 2 1 0 - - - - - - - -
;Put 6 7 shifted right three times in left byte
;      - - - 7 6 2 1 0 - - - - - - - -
;Put 3 4 5 shifted left two times in right byte
;      - - - 7 6 2 1 0 5 4 3 - - - - -
;
;-->   - - - 7 6 2 1 0 5 4 3 - - - - -
;            [-] [---] [---]






; Given the specified memory address in video memory hl, this method increments it
; so that the next memory address will be immediately to the right of the previous one on the screen,
; or if the end of a row has been reached, it will be on the next visible row.
;
; (simple)
;
; Parameters:
;       hl: The memory address to increment
; Modifies:
;       a, hl
; Returns:
;       hl: The specified memory address incremented by one.
generic_incrementVideoPosition:
    ld a, l
    and 31
    cp 31
    jp z, genericIf_incrStage1
        ; If x position bits (l register 0,1,2,3,4) are not yet 11111, then simply increment x position bits.
        inc l
    ret
    
    ; If x position bits are 11111:
    genericIf_incrStage1:
    ld a, h
    and 7
    cp 7
    jp z, genericIf_incrStage2
        ; If y position bits 0,1,2 (h regitster 0,1,2) are not yet 111:
        ld a, l ; Then reset x position bits to zero
        sub 31
        ld l, a
        inc h   ; And increment y position bits 0,1,2 (h register 0,1,2)
    ret
    
    ; If y position bits 0,1,2 (h register 0-2) are 111:
    genericIf_incrStage2:
    ld a, l
    and 0xe0
    cp 0xe0
    jp z, genericIf_incrStage3
        ; If y position bits 3,4,5 (l register 5,6,7) are not yet 111:
        inc l   ; Then reset x position bits to zero, AND increment y position bits 3,4,5 (l register 5,6,7).
        ld a, h
        sub 7   ; Reset y position bits 0,1,2 (h register 0,1,2)
        ld h, a
    ret
    
    ; If y position bits 3,4,5 (l register 5,6,7) are 111:
    genericIf_incrStage3:
    ld a, h
    and 24
    cp 24
    jp z, genericIf_incrStage4
        ; If y position bits 6,7 (h register 3,4) are not yet 11:
        ld l, 0 ; Reset x position bits to zero AND reset y position bits 3,4,5 (l register 5,6,7) to zero.
        inc h   ; Reset y position bits 0,1,2 (h register 0,1,2) to zero, AND increment y position bits 6,7 (h register 3,4)
    ret
    
    ; If y position bits 6,7 (h register 3,4) are 11:
    genericIf_incrStage4:
    
    ld hl, SCREEN_MEMSTART ; Reset the memory address back to the start.
ret














