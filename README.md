
# Wilso-Pong

This is an implementation of Pong for the ZX Spectrum, written in Zilog Z80 Assembly. I designed this in December 2017,
and it was my first game for the ZX Spectrum.

Provided is the source code for the game, in the src/ directory, and two files: pong.wav and pong.tap. The .tap file can be loaded
into any ZX Spectrum emulator, and the .wav file can be used to load the game into a real ZX Spectrum.

To compile, there is a makefile provided. The makefile will do the following:

 * Assemble this project, using the [z80asm assembler](https://packages.debian.org/stretch/z80asm).
 * Convert the resultant .bin file into a .tap file, using [bin2tap](http://metalbrain.speccy.org/bin2tap.c). bin2tap can be compiled with
   gcc, for example: `gcc bin2tap.c -o bin2tap`.
 * Generate a wav file from this, for the ZX Spectrum, using [zxtap-to-wav](https://github.com/raydac/zxtap-to-wav). The github page
   contains documentation about how to compile this.

The makefile assumes that z80asm is installed, the bin2tap executable exists as /opt/bin2tap, and the zxtap-to-wav
executable exists as /opt/zxtap-to-wav.

To compile the project into a .tap file, navigate to the src directory, then run `make`. To also generate the .wav file, run `make noise`.

**Acknowledgements:**
 * Andrew Ackerley, for the design of the menu/logo screen.

# Screenshots

The game's main menu/logo screen.

![Image](screenshots/menu.png)

The game mode selection menu.

![Image](screenshots/gamemodeSelection.png)

A screenshot during a game of pong.

![Image](screenshots/inGame.png)
